#! /bin/bash
# Сборка интерфейса
# cd client && npm install
# npm run deploy

# Копирование интерфейса - для стенда, поменять на свои
# rm -rf mydir
#cd client/app
#mkdir app/templates

git pull
cd client
npm run deploy
cd ..
cp -f /home/vsemagush/active_manager/client/dist/index.html /home/vsemagush/active_manager/app/templates
cp -fr /home/vsemagush/active_manager/client/dist/static/ /home/vsemagush/active_manager/app
gcloud app deploy