const mocks = {
  'auth': { 'POST': { token: 'This-is-a-mocked-token' } },
  'user/me': { 'GET': { name: '', title: '' } }
}

const apiCall = ({url, method, data}) => new Promise((resolve, reject) => {
  setTimeout(() => {
    try {
      // кастыли по пробросу текущего пользователя в шапку
      if (data) {
        mocks['user/me']['GET'].name = data.username
        window.username = data.username
      }

      // after login
      if (mocks['user/me']['GET'].name) {
        mocks['user/me']['GET'].name = window.username
      }

      resolve(mocks[url][method || 'GET'])
      console.log(`Mocked '${url}' - ${method || 'GET'}`)
      console.log('response: ', mocks[url][method || 'GET'])
    } catch (err) {
      reject(new Error(err))
    }
  }, 1000)
})

export default apiCall
