/**
 * Модуль определяет базовые роуты для страницы
 */
import Vue from 'vue'
import Router from 'vue-router'
import Home from 'components/home'
import Game from 'components/home/game'
import Active from 'components/home/active'
import Account from 'components/account'
import Login from 'components/login'
import store from '../store'

Vue.use(Router)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/game',
      name: 'Game',
      component: Game,
    },
    {
      path: '/active',
      name: 'Active',
      component: Active,
    },
    {
      path: '/account',
      name: 'Account',
      component: Account,
      beforeEnter: ifAuthenticated,
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: ifNotAuthenticated,
    }
  ]
})
