const h337 = require('heatmap.js')
const common = {

  /**
   * теплокарта по данным из game
   */
  heatMapClick: function () {
    window.mark_game_start = false
    window.mark_temp_kost = true
    clearInterval(window.counter_v) // остановка счётчика
    document.querySelector('.game-wrapper').onclick = undefined

    document.querySelector('.heatmap').classList.add('hide')
    document.querySelector('.game-content').innerHTML = ''
    var heatmapInstance = h337.create({
      container: document.querySelector('.game-content'),
      radius: 50,
      minOpacity: 0.07,
      blur: 0.8,
      legend: {
        position: 'br',
        title: 'Example Distribution'
      }
    });
    heatmapInstance.addData(window.dataGameCard)
  }
};

export default common
