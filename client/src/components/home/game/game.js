/**
 * Фунция создания падающих объектов для отслеживания кликов
 */
import heatmap from './heatmap'
window.lvl = 1
window.st = window.localStorage.getItem('statistics') ? window.localStorage.getItem('statistics') : '[]'
window.localStorage.setItem('statistics', window.st)
function Start () {
  document.querySelector('.heatmap').classList.add('hide')

  // base64 картинки
  let height = document.getElementById('game-content').clientHeight
  let width = document.getElementById('game-content').clientWidth
  let col = 5 + Math.round(lvl / 2)   // количество снежинок
  let fallSpeed = (21 - lvl)// скорость снежинок

  let amp = []
  let x = []
  let y = []
  let stx = []
  let sty = []
  let delta = []
  let obj = []

  // Размер навигации и кнопок
  window.dataGameCard = [] // массив кликов
  document.querySelector('.game-content').innerHTML = ''
  document.querySelector('.game-content').onclick = function (e) {
    // добавление клика в массив
    if (window.mark_game_start) {
      self.count_miss += 1
      let xVal = e.layerX
      let yVal = e.layerY
      let val = 20
      for (let i = 0; i < window.dataGameCard.length; i++) {
        if (window.dataGameCard[i].x == xVal && window.dataGameCard[i].y == yVal) {
          window.dataGameCard[i].value = window.dataGameCard[i].value + 1
          val = 0
        }
      }

      window.dataGameCard.push({
        x: xVal,
        y: yVal,
        value: val
      })
    }
  }

  let result = ''
  for (let i = 0; i < col; ++i) {
    amp[i] = Math.random() * 19
    x[i] = Math.random() * (width - amp[i] - 35)
    y[i] = Math.random() * (width - amp[i])
    stx[i] = 0.03 + Math.random() * 0.25
    sty[i] = 2 + Math.random()
    delta[i] = 0

    result +=
      '<img id="sn' +
      i +
      '" class="tensor" style="position:absolute;' +
      'z-index: ' +
      i +
      '; visibility:visible; top:-70px; left:-70px;"' +
      " width='75px' " +
      "src='https://tensor.ru/hack/images/logo.png" +
      "' border=0>"
  }
  function addStats () {
    let outStatistics = JSON.parse(window.localStorage.getItem('statistics'))
    outStatistics.push({name: window.localStorage.getItem('user-name'), lvl: window.lvl, time: window.totalTime })
    window.localStorage.setItem('statistics', JSON.stringify(outStatistics))
    window.totalIime = 0
  }

  function displayBlock () {
    addStats()
    setTimeout(function () {
      heatmap.heatMapClick()
    }, 1)
    if (self.top_time == 0 || parseInt(self.count_total.split(':').join('')) < parseInt(self.top_time.split(':').join(''))) {
      self.top_time = self.count_total
      window.localStorage.setItem('topTime', self.count_total)
    }
    document.getElementById('inc').removeAttribute('disabled')
    document.getElementById('redu').removeAttribute('disabled')
  }

  let self = this

  if (window.localStorage.getItem('topTime') !== null) {
    window.ggwp = window.localStorage.getItem('topTime')
  } else {
    window.localStorage.setItem('topTime', 10000)
    window.ggwp = 0
  }

  self.count_click = 0
  self.index_i = 0
  let countСol = col

  function clickElement (tempobj) {
    // обработчик клика по птичке
    self.count_click += 1
    self.count_miss -= 1
    tempobj.currentTarget.style.display = 'none'
    countСol--
    if (window.dataGameCard.length) {
      // помечаем клик как попадание
      window.dataGameCard[window.dataGameCard.length - 1].target = 1
    }

    if (countСol === 0) {
      // после убийства всех птичек перейти в функцию
      self.count_miss += 1
      displayBlock()
    }
  }

  document.getElementById('game-content').innerHTML = result

  for (let i = 0; i < col; ++i) {
    obj[i] = document.getElementById('sn' + i)
    self.index_i = i
    obj[i].addEventListener('click', clickElement)
  }

  /**
   * Функция имитирующая плавное опускание данных
   * @param {Array} arr
   */
  function flake (arr) {
    for (let i = 0; i < col; ++i) {
      y[i] += sty[i]
      if (screen.width < 1025) {
        height = screen.height * 0.23
        if (screen.width < 400) {
          height = screen.height * 0.35
        }
      } else {
        height = screen.height * 0.45
      }
      if (y[i] > (screen.height - height)) {
        x[i] = Math.random() * (width - amp[i])
        y[i] = Math.random() * (width - amp[i])
      }
      delta[i] += stx[i]

      arr[i].style.top = y[i] + 'px'
      arr[i].style.left = x[i] + amp[i] * Math.sin(delta[i]) + 'px'
    }
  }

  window.flakeInt = setInterval(function () {
    flake(obj)
  }, fallSpeed)
}

export default Start
