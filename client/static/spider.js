/* eslint-disable no-constant-condition */
/* global $, detect */
// eslint-disable-next-line no-unused-vars
(function (w) {
  // todo нужен клиентский коллектор данных
  const _blPoint = {
    send: (pointName, data) => {
      let user1 = detect.parse(navigator.userAgent)
      let userName = localStorage.getItem('user-name')
      let resultData = JSON.stringify({
        user: userName,
        location: location.href,
        time: new Date(),
        data: data,
        browser: user1.browser.family,
        device: user1.device.type
      })
      navigator.sendBeacon('/' + pointName, resultData)
    }
  }
  const _private = {
    /**
     * Получить размер окна
     */
    getPageSize: () => {
      let doc = document.body
      return {
        x: doc.clientHeight,
        y: doc.clientWidth
      }
    },
    /**
     * Обработка любых кликов на странице
     * https://developer.mozilla.org/ru/docs/Web/API/MouseEvent
     * @param {mouseEvent} mouseEvent
     */
    clickHandler: (mouseEvent) => {
      let target = mouseEvent.target
      let result = {
        'x': mouseEvent.screenX,
        'y': mouseEvent.screenY,
        'screen_x': _private.getPageSize().x,
        'screen_y': _private.getPageSize().y,
        'element_type': target.nodeName,
        'class_list': target.classList.value
      }

      _blPoint.send('user_click', result)
    },

    /**
     * Обработка загрузки страницы
     */
    loadHandler: (event) => {
      let result = {
        'load-time': event.timeStamp
      }
      _blPoint.send('first_load', result)
    },

    /**
     * При смене url залогируем
     */
    changePageHandler: (event) => {
      let oldURL = window.location.pathname
      function checkURL (currentURL) {
        if (currentURL && currentURL !== oldURL) {
          _blPoint.send('change_page', '')
          oldURL = currentURL
        }
      }

      setInterval(function () {
        checkURL(window.location.pathname)
      }, 500)
    }
  }

  w.addEventListener('load', _private.loadHandler, true)
  w.addEventListener('click', _private.clickHandler, true)

  _private.changePageHandler()

  if (false) {
    $.ajax({
      type: 'POST',
      url: '/active',
      crossDomain: true,
      data: '',
      success: function (data) {
        debugger
      }
    })
  }
})(window)
