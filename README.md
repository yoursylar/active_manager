# Active_manager

Ответственные A.Antonov, M.Slastuhin and R.Fomichev

# STEP BY STEP

## 1. Стартовые действия
- Регистрация на gmail (https://www.google.com/gmail/)
- Регистрация на bitbucket (https://bitbucket.org/)
- Регистрация на trello (https://trello.com)
- Зайти в вк и найти кураторов: https://vk.com/id9407406 и https://vk.com/id31131609
- Войти в беседу: https://vk.com/im?invite_chat_id=8589934592216742286&invite_hash=STIjrYyeysqzEw==
- Получить приглашение в чат и отписать ФИО + gmail почту (на случай не соответствия аккаунта и фактических данных)
- После регистрации добавиться в репозиторий https://bitbucket.org/yoursylar/active_manager
- На диске "С" (или другом) создать папку tensor (в ней будет храниться файлы проекта)


## 2 . Установка окружение (детали в соответствующих подразделах)
- download GIT https://git-scm.com/downloads
- download Python 3.7 https://www.python.org/downloads/  (для Backend)
- download IDE:
    * Frontend (один из двух):
        * WebStorm (https://www.jetbrains.com/pycharm/)
        * VS (https://code.visualstudio.com/)
    * Backend:
        * Pycharm Free Community (https://www.jetbrains.com/pycharm/)
- download https://www.google.com/chrome/
- download https://nodejs.org/en/
- download https://www.openscg.com/postgresql/installers/  (для Backend)
- download REST API Client https://www.getpostman.com/apps (для проверки запросов)

### Python
Проверяем наличие python3 на компьютере - в консоли набираем python/python3 (в зависимости от наличия на компьютере нескольких версий python).

При отсутствии ставим с сайта (не забываем проставить галку добавления в PATH)
https://www.python.org/downloads/windows/
  
### GIT
После установки 

`git config --global user.name "YOUR NAME"`

`git config --global user.email YOUR_MAIL@M.COM`

### Pycharm Free Community (https://www.jetbrains.com/pycharm/)
Настройки установки:
- Добавляем ссылку на 64-bit launcher
- Добавляем ассоциацию с py-файлами

Клонирование репозитория через PyCharm:
- В верхнем меню PyCharm выбираем VCS->Checkout from Version Control->Git
- Вставляем путь до репозитория (берем с bitbucket.org) + вводим данные для входа(если запросят) + указываем путь до ранее созданной папки tensor.
- Дожидаемся окончания клонирвания и проверяем, что в указанной папке появились файлы проекта.

Настройка проекта
- Верхнее меню - File - Settings
- В панели слева расскрываем раздел Project и выбираем Project Interpreter
- Панель справа - поле Project Interpreter - выбираем установленный интерпритатор Python 3.7
- Если нужного интерпритатора нет, то жмем на шестеренку справа - жмем add - в новом окне в разделе слева выбираем System Interpreter и в поле Interpreter выбираем нужный интерпритатор - жмем Ок
- Нажимаем кнопку "+" (расположена по правому краю таблицы ниже)
- В открывшемся окне последовательно находим и устанавливаем (Install Package) пакеты из списка ниже.
	* pip (если отсуствует)
	* flask
	* psycopg2 (Внимание! нужна версия >= 2.7 и < 2.8)
	* psycopg2-binary (Внимание! ставим ту же весрию, что и у psycopg2)
- После успешной установки всех пакетов закрываем текущее окно
- Проверяем, что в таблице появились установленные пакеты - жмем Apply и OK

Проверка Git
- 

Деплой Frontend-части
- переходим в терминал (нижняя панель инструментов)
- `cd /client`
- `npm i & npm run deploy`
- после завершения деплоя копируем client/dist/index -> app/templates
- копируем client/dist/static -> app/

Проверка корректности установки и настройки
- Pycharm - окно Project - в корне проекта находим файл main-example.py - на нем вызываем контекстное меню - Run 'Flask(main-example)' 
- В окне Run появится информация о запуска сервера Flask - переходим по указаному в окне адресу
- Проверяем, что открылась страничка с 'Hello World!'

Запуск отладочного сервера и проверка точек останова:
- Pycharm - окно Project - в корне проекта находим файл main-test.py - на нем вызываем контекстное меню - Debug 'main-test' 
- В окне Debug появится информация о запуска сервера Flask - переходим по указаному в окне адресу
- Ставим точку останова в app\routes.py на `return 'Hello World!`
- В браузере на страничке нашего приложение добавляем в адрес "/hello" - Enter - В PyCharm должна сработать точка останова - продолжаем выполнение программы (F9) - В браузере отображается страница Hello World!


Ведение разработки:
- Создаем отдельную ветку в PyCharm для своих изменений (возможно сделать через интерфейс справа внизу).
- Локальные изменения - необходимо коммитить в свою ветку. Слить свою ветку в master можно, отправив ее на удаленный сервер и создав Pull Request на сайте bitbucket-a (ответственные за репозиторий одобряют PR).

### Установка СУБД
+Клиент для взаимодействия с ней (лучший вариант для постоянного использования - устанавливать прямо с postgresql.com, но нам нужен быстрый вариант с удобным клиентом - pgadmin3)
https://www.openscg.com/bigsql/postgresql/

- Ставим 11.1
- Пароль для СУБД - postgres (для простоты доступа к ресурсам).

- Документация по PostgreSQL на русском:
https://postgrespro.ru/docs/postgrespro/11/index

- Открываем pgadmin3 LTS. Добавляем соединение на localhost 5432 postgres/postgres (на ошибки не обращаем внимания).
        - Добавляем новую базу данных с названием activity


# Стартовые действия по разработке UI
- `cd /client`
- `npm i`
- `npm run dev`
- open in browser `localhost:<port>/am`
- открыть консоль найти файл spider.js
- поставить точку в функцию send и выполнить клик
- код останавливается перед отправкой запроса
  
# GIT
### 4. Порядок действий при создании pull-request для Git
- маска наименование ветки: fix-name_what-i-do
- создаем ветку от master: `git checkout -b fix-max_expample-merge`
- вносим изменений
- `git commit -m "Add an author comment"` \ ctrl + k (WebStorm, PyCharm)
- `git push origin` \ ctrl + shift + p (WebStrom, PyCharm)  
- открываем https://bitbucket.org/yoursylar/active_manager/pull-requests/
- `create a pull request`
- заполняем данные + `create pull request`
- пинаем кураторов чтобы слили изменения
- `git checkout master` - возвращаемся на корень
- `git pull ` - обновляемся
- теперь локально все свежо и можно пилить фичи дальше!!!

# Доп информация

## Создание билда бумеранга
- `git clone https://github.com/akamai/boomerang.git`
- `npm install -g grunt-cli`
- `npm install grunt --save-dev`
- https://stackoverflow.com/questions/31694685/make-gulp-uglify-not-mangle-only-one-variable
/boobmerang/Gruntfile.js нужно заменить в файле `except->reserved`
- `grunt clean build`
- ./build/boomerang-<version>.min.js - build file with your plugins

## для OSX нужно
- `xcode-select --install`

# Разворот боевого билда
- перейти на https://console.cloud.google.com/cloudshell/editor?folder&organizationId&project=activemanager
- консоль google cloud
- В одну команду: ./deploy.sh

### По шагам
  - перейти в папку active_manager
  - git checkout master 
  - git status - убедиться что все ок (только две директории созданные руками через deploy.sh)
  - git pull - если нужно обновить
  - cd client 
  - npm run deploy - получим свежий билд
  - обновим ресурсы на собранный билд (см deploy.sh)
  - gcloud app deploy (разворот может занимать до 2х минут)
  - gcloud app browse - получить url (https://activemanager.appspot.com/)
  - gcloud app logs tail -s default - вотчер логов

### Боевая БД
- activity - postgres/1qaz2wsx

### Стартовые действия по разработке BL (Из териминала)
- install python3+ https://www.python.org/downloads/windows/
- install pip3 https://pythonworld.ru/osnovy/pip.html
- `pip3 install flask`
    * Проверяем корректность установки - в консоли набираем python. В python-интерпретаторе вводим import flask
- `pip3 install psycopg2`
- `pip3 install psycopg2-binary`
- `cd /client`
- `npm i & npm run deploy`
- копируем client/dist/index -> app/templates
- копируем client/dist/static -> app/
- `cd ../`
- `python3 main-example.py` - проверяем что пример стартанул
- `python3 main-test.py` - отладочный модуль для разработки