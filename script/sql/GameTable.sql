

CREATE TABLE public.game_time
(
  user_player text,
  lvl text,
  global_time timestamp with time zone,
  game_time text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.game_time
  OWNER TO postgres;
