-- Table: public."UserStat"

-- DROP TABLE public."UserStat";

CREATE TABLE public."UserStat"
(
  "Click" boolean,
  "Move" boolean,
  "UserId" integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."UserStat"
  OWNER TO postgres;