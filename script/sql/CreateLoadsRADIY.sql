-- Table: public.loads

-- DROP TABLE public.loads;

CREATE TABLE public.loads_1
(
  location text,
  user_name text,
  device text,
  load_time double precision,
  time_ins timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.loads_1
  OWNER TO postgres;