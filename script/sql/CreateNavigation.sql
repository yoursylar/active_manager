-- Table: public."Navigation"

-- DROP TABLE public."Navigation";

CREATE TABLE public."Navigation"
(
  """c.e""" text,
  """c.tti.m""" text,
  """rt.bstart""" text,
  """u""" text,
  """v""" text,
  """rt.si""" text,
  """rt.ss""" text,
  """rt.sl""" text,
  """vis.st""" text,
  """ua.plt""" text,
  """ua.vnd""" text,
  """pid""" text,
  """n""" text,
  """rt.tstart""" bigint,
  """rt.end""" bigint,
  """rt.start""" text,
  """http.initiator""" text,
  """c.l""" text,
  """c.f""" text,
  """c.f.d""" text,
  """c.f.m""" text,
  """c.f.s""" text,
  """c.s""" text,
  """c.s.p""" text,
  """c.s.y""" text,
  """c.s.d"""  text,
  """c.m.p"""  text,
  """c.m.n"""  text,
  """c.ttfi""" text,
  """c.fid"""  text,
  """c.lb"""  text,
  """User"""  text,
  """navtime""" timestamp
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Navigation"
  OWNER TO postgres;