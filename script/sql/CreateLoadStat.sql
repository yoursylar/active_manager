-- Table: public."LoadStat"

-- DROP TABLE public."LoadStat";

CREATE TABLE public."LoadStat"
(
  """c.e""" text,
  """c.tti.m""" text,
  """rt.start""" text,
  """rt.bmr""" text,
  """rt.tstart""" text,
  """rt.bstart""" text,
  """rt.end""" text,
  """t_resp""" text,
  """t_page""" text,
  """t_done""" text,
  """t_other""" text,
  """rt.tt""" text,
  """rt.obo""" text,
  """pt.fp""" text,
  """pt.fcp""" text,
  """u""" text,
  """v""" text,
  """rt.si""" text,
  """rt.ss""" text,
  """rt.sl""" text,
  """vis.st""" text,
  """ua.plt""" text,
  """ua.vnd""" text,
  """pid""" text,
  """n""" text,
  """c.tti.vr""" text,
  """c.lt.n""" text,
  """c.lt.tt""" text,
  """c.lt""" text,
  """c.f""" text,
  """c.f.d""" text,
  """c.f.m""" text,
  """c.f.l""" text,
  """c.f.s""" text,
  """sb""" text,
  """User""" integer
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."LoadStat"
  OWNER TO postgres;