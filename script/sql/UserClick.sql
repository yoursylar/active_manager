CREATE TABLE public.user_click
(
  user_name text,
  page text,
  type_elem text,
  x real,
  y real,
  x_size real,
  y_size real,
  device text,
  class_elem text,
  browser text,
  devide text,
  res_time timestamp with time zone
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.user_click
  OWNER TO postgres;
