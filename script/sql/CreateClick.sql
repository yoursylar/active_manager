-- Table: public."Click"

-- DROP TABLE public."Click";

CREATE TABLE public."Click"
(
  """user""" text,
  """pageid"""  text,
  """pageurl"""  text,
  """element"""  text,
  """id"""  text,
  """class"""  text,
  """document_height"""  integer,
  """document_width"""  integer,
  """viewport_height"""  integer,
  """viewport_width""" integer,
  """x"""  integer,
  """y"""  integer,
  """clickdatetime""" timestamp
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public."Click"
  OWNER TO postgres;