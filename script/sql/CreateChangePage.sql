-- Table: public.change_page

-- DROP TABLE public.change_page;

CREATE TABLE public.change_page
(
  time_change timestamp with time zone,
  user_name text,
  page_to text,
  page_from text,
  device text
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.change_page
  OWNER TO postgres;