from app import app
from flask import request
from app import Parsing
from app import send_data_change_page


@app.route('/change_page', methods=['POST'])
def changePage():
    conn = None
    try:
        conn = app.pool.getconn()
        query = '''INSERT INTO change_page("time_change", "user_name", "page_to", "device")
        values (%s::timestamptz, %s::text, %s::text, %s::text) '''
        data = Parsing.parsing_request_from(request.data)
        with conn.cursor() as curs:
            curs.execute(query,
                         (data.get('time'),
                          data.get('user'),
                          data.get('location'),
                          data.get('device')
                          ))
        conn.commit()
    finally:
        if conn:
            app.pool.putconn(conn)
    send_data_change_page.send_change_data_info()
    return 'true'
