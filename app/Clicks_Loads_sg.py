import datetime
import json
from app import app
from flask import request
from app import Parsing
from app import get_location_loadtime_timeins_sg



@app.route('/first_load', methods=['POST'])
def clicks_loads():
    conn = None
    try:
        conn = app.pool.getconn()
        query = '''INSERT INTO "loads"("location", "time_ins", "user_name", "device", "load_time")
        values (%s::text, %s::timestamptz, %s::text, %s::text, %s::double precision) '''
        data = Parsing.parsing_request_from(request.data)
        with conn.cursor() as curs:
            curs.execute(query,
                         (data.get('location'),
                          data.get('time'),
                          data.get('user'),
                          data.get('device'),
                          data.get('data').get('load-time')
                          ))
        conn.commit()
    finally:
        if conn:
            app.pool.putconn(conn)

    get_location_loadtime_timeins_sg.get_location_loadtime_timeins()
    return 'true'
