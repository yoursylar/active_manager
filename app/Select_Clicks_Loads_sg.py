import datetime
import json
from app import app
from flask import request
from app import Parsing



def select_clicks_loads():
    conn = None
    try:
        conn = app.pool.getconn()
        query = 'select * from loads'
        with conn.cursor() as curs:
            curs.execute(query)
            temp = curs.fetchall()
        conn.commit()
    finally:
        if conn:
            app.pool.putconn(conn)
    return 'true'
