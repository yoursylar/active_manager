import json
from app import app
from flask import request, jsonify


@app.route('/get_browser', methods=['GET', 'POST'])
def get_browser():
    conn = None
    data = []
    dic = dict()
    dic1 = dict()
    lst2=[]
    dic2=dict()
    browsers=[]
    count={'Chrome': 0,
           'Safari': 0,
           'Mobile Safari':0}
    try:
        conn = app.pool.getconn()
        query = 'select user_name, browser from user_click group by user_name,browser'
        with conn.cursor() as curs:
            curs.execute(query)
            lst1 = curs.fetchall()
            conn.commit()
            for i in lst1:
                lst2.append(list(i))
            for i in lst2:
                if (i[1] not in count):
                    i[1] = 'Chrome'
                count[i[1]] += 1

            dic['name'] = 'Chrome'
            dic['y'] = count['Chrome']
            data.append(dic)
            dic1['name'] = 'Safari'
            dic1['y'] = count['Safari']
            data.append(dic1)
            dic2['name']='Mobile Safari'
            dic2['y']=count['Mobile Safari']
            data.append(dic2)
    finally:
        if conn:
            app.pool.putconn(conn)
    return jsonify(data)
    # return json.dumps(data)
