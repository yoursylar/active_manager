import json
from app import app
from flask import request, jsonify


@app.route('/get_devices', methods=['GET', 'POST'])
def get_devices():
    conn = None
    data = []
    count = {'Desktop': 0,
             'Mobile': 0,
             'Table': 0}
    dic = dict()
    dic1 = dict()
    dic2 = dict()
    lst2=[]
    try:
        conn = app.pool.getconn()
        query = 'select user_name, device from user_click group by user_name,device'
        with conn.cursor() as curs:
            curs.execute(query)
            lst1 = curs.fetchall()
            conn.commit()
            for i in lst1:
                lst2.append(list(i))
            for i in lst2:
                if (i[1] not in count):
                    i[1] = 'Mobile'
                count[i[1]] += 1

            dic['name'] = 'Desktop'
            dic['y'] = count['Desktop']
            data.append(dic)
            dic1['name'] = 'Mobile'
            dic1['y'] = count['Mobile']
            data.append(dic1)
            dic2['name'] = 'Table'
            dic2['y'] = count['Table']
            data.append(dic2)

    finally:
        if conn:
            app.pool.putconn(conn)
    return jsonify(data)
    # return json.dumps(data)
