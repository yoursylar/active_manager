from app import app
from flask import jsonify


@app.route('/get_popular_pages', methods=['GET', 'POST'])
def send_change_data_info():
    conn = None
    bene_dict = ['name', 'data']
    data_change_in = []
    data_change_out = []
    try:
        conn = app.pool.getconn()
        query = '''SELECT page_to, count(page_to) from change_page group by page_to'''
        with conn.cursor() as curs:
            curs.execute(query)
            temp_count = curs.fetchall()
        for i in temp_count:
            data_change_in.append(list(i))
        for i in data_change_in:
            i[1] = [i[1]]
        conn.commit()
        for i in data_change_in:
            data_change_out.append({key: value for key, value in zip(bene_dict, i)})
    finally:
        if conn:
            app.pool.putconn(conn)
    return jsonify(data_change_out)
